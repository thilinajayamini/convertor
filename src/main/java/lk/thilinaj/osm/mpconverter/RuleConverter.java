/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 *
 * @author Thilina Jayamini
 */
public class RuleConverter {

    private void createcfg(String fileName) throws Exception {

        BufferedReader br;
        try (FileReader fr = new FileReader(fileName)) {
            br = new BufferedReader(fr);
            try (FileWriter fwn = new FileWriter("Rule-" + fileName, false);
                    FileWriter fwErr = new FileWriter("RuleErr-" + fileName, false)) {
                String line;
                StringBuilder sb;
                String type = null;
                String levelH = null;
                String levelL = null;

                while (null != (line = br.readLine())) {

                    if (line.startsWith("- cond")) {
                        sb = new StringBuilder();

                        while (!(line = br.readLine().trim()).startsWith("action:")) {
                            sb.append(line.replace("-", "").replace(" ", "")).append(";");
                        }

                        sb.replace(sb.length() - 1, sb.length(), "");

                        while (!(line = br.readLine().trim()).equals("")) {
                            if (line.trim().startsWith("type:")) {
                                type = line.split(":")[1];
                            }

                            if (line.trim().startsWith("level_h:")) {
                                levelH = line.split(":")[1];
                            }

                            if (line.trim().startsWith("level_l:")) {
                                levelL = line.split(":")[1];
                            }
                        }
                        if (type != null) {
                            sb.append(",").append(type.trim());
                        } else {
                            throw new Exception("No MP type found");

                        }

                        if (levelH != null) {
                            sb.append(",").append(levelH.trim());
                        } else {
                            System.out.println("INFO: No MP High Level  found for " + type);

                        }

                        if (levelL != null) {
                            sb.append(",").append(levelL.trim());
                        } else {
                            System.out.println("INFO: No MP Low Level  found for " + type);

                        }
                        //sb.replace(sb.length() - 1, sb.length(), "\n");
                        postProcessor(fwn, fwErr, sb.toString());
                        type = null;
                        levelH = null;
                        levelL = null;

                    }

                }

            }
        }
        br.close();

    }

    private void postProcessor(FileWriter fwn, FileWriter fwa, String line) throws Exception {

        String[] parts;
        String[] subParts;
        java.util.ArrayList<String> lst1 = new ArrayList<>(), lst2 = new ArrayList<>(), lst3 = new ArrayList<>();
        String key;

        if (line.contains("!") || line.contains("*")) {
            fwa.append(line);
            fwa.append("\n");
        } else {
            if (line.split(",")[0].contains("|")) {
                parts = line.split(",");

                subParts = parts[0].split(";");

                for (String subPart : subParts) {
                    if (subPart.contains("|")) {
                        key = subPart.split("=")[0];
                        for (String s : subPart.split("=")[1].split("\\|")) {//lines cointaining | oparator
                            lst2.add(key + "=" + s);//create multiple row s for key with each option
                        }
                    } else {
                        lst1.add(subPart);//if no | just add to output
                        continue;
                    }
                    if (lst1.isEmpty()) {//first time adding
                        lst1.addAll(lst2);
                    } else {//creating combinations with each one 
                        lst3.addAll(lst1);
                        lst1.clear();
                        lst3.stream().forEach((itm3) -> {
                            lst2.stream().forEach((itm2) -> {
                                lst1.add(itm3 + ";" + itm2);
                            });
                        });
                    }
                    lst2.clear();
                    lst3.clear();
                }

                appendToFile(parts, fwn, lst1);

            } else {
                fwn.append(line).append("\n");
            }
            lst1.clear();
        }

    }

    private static void appendToFile(String[] parts, FileWriter fw, ArrayList<String> lst) throws Exception {
        for (String itm1 : lst) {

            fw.append(itm1);
            for (int i = 1; i < parts.length; i++) {
                fw.append(",");
                fw.append(parts[i]);
            }
            fw.append("\n");
        }
    }

    /*
     public static void main(String[] args) throws Exception {
     //new RuleConvertor().createcfg("cityguide-nodes.yml");
     new RuleConvertor().createcfg("cityguide-roads.yml");
     //new RuleConvertor().createcfg("cityguide-polygons.yml");

     }*/
     }
