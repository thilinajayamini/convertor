/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class Config {

    static String mapName;
    static String mapCode;
    static String inputFile;
    static String outputFile = Config.inputFile;
    static String polyFile;
    static String imgHeader;

    static boolean addressing = false;
    static boolean seaBackGround = true;
    static boolean enableLayes = true;
    static boolean splitRoads = true;
    static boolean fixSelfIntersection = true;
    static boolean applyRestrictions = false;
    static boolean applySigns = false;
    static boolean applyBarriers = true;
    static boolean overrideCity = true;

    static int maxRoadNodes = 60;
    
    static String rNode;
    static String rLine;
    static String rPolygon;
    static String rRoad;

}
