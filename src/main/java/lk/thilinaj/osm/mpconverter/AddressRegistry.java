/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter;

import lk.thilinaj.osm.parser.Coordinate;
import lk.thilinaj.osm.mpconverter.mp.MPRegistry;
import lk.thilinaj.osm.mpconverter.mp.MPSection;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Thilina Jayamini
 */
public class AddressRegistry implements MPRegistry {

    private final java.util.List<MPSection> toProcess;

    private String country = "Sri Lanka";
    private final Map<String, Region> districts;

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public AddressRegistry() {
        toProcess = new java.util.ArrayList<>();
        districts = new java.util.HashMap<>();
    }

    public void addPlace(MPSection mp) {
        this.toProcess.add(mp);
    }

    public void addDistrict(String name, List<List<Coordinate>> boudary) {
        this.districts.put(name, new Region(name, boudary.get(0), null));
    }

    @Override
    public void fillAddress(MPSection ms) {

        if (ms.isPlace() || districts.isEmpty()) {
            return;
        }

        double[] postition = ms.getCoordinate().getPossition();

        Region d = districts.entrySet().stream().filter((entry -> entry.getValue().isInside(postition))).findAny().get().getValue();

        String place;
        if ((Config.overrideCity || ms.getAttribute(Const.CITY) == null) && ms.isValidAddress()) {

            place = findRelavantCityOrSuburb(d, postition);
            ms.addAttribute(Const.CITY, place);
        }
        ms.addAttribute(Const.REGION, d.getName());
        ms.addAttribute(Const.COUNTRY, country);
    }

    public void build() {
        if (this.districts.isEmpty()) {
            return;
        }
        toProcess.stream().forEach((ms) -> {

            if (ms.getAttribute(Const.NAME) == null || ms.getData() == null || ms.getData().isEmpty() || ms.getData().get(0).isEmpty()) {
                System.out.println("Ohh shit error");
                ms.getComments().stream().forEach(s -> System.out.println(s));
            } else {

                if (ms.getAttribute(Const.REGION) == null) {
                    fillRegionAndAddRegistry(ms);
                } else {

                    if (ms.getData().get(0).size() > 1) {
                        addToDistricPlacessWithBorders(ms, new Region(ms.getAttribute(Const.NAME), ms.getData().get(0), ms.getCoordinate().getPossition()));

                    } else {
                        addToDistrictPlaces(ms);
                        ms.addAttribute(Const.IS_A_CITY, "Y");
                    }
                }

                ms.addAttribute(Const.COUNTRY, country);
            }

        });

        groupSuburbsWithCities();

    }

    private void addToDistrictPlaces(MPSection ms) {
        Region d = districts.get(ms.getAttribute(Const.REGION));
        if (d == null) {
            d = searchDistrict(ms.getCoordinate().getPossition());
        }
        if (d != null) {
            d.places.put(ms.getCoordinate(), new Point(ms.getAttribute(Const.NAME), ms.getCoordinate().getPossition()));

            if (ms.getAttribute(Const.TYPE).equals("0x1F00")) {
                d.suburbs.add(ms);
            }
        }

    }

    private void addToDistricPlacessWithBorders(MPSection ms, Region a) {
        Region d = districts.get(ms.getAttribute(Const.REGION));
        if (d == null) {
            d = searchDistrict(ms.getCoordinate().getPossition());
        }
        if (d != null) {
            Region put = d.placesWithBordes.put(ms.getCoordinate(), a);

            if (put != null) {
                System.out.println("ooo duplicate city " + ms.getAttribute(Const.NAME));
            }

            switch (ms.getAttribute(Const.TYPE)) {
                case "0x1F00"://SUBURB
                    d.suburbs.add(ms);
                    break;
                case "0x011"://SUBURB
                    d.suburbs.add(ms);
                    ms.addAttribute(Const.TYPE, "0x01");//recorecting to standerd code
                    break;
                case "0x01"://CITY - polygon
                case "0x02"://TOWN - polygon
                    d.cities.add(ms);
                    break;
            }
        }
    }

    private void fillRegionAndAddRegistry(MPSection ms) {
        double[] p;
        Region dis;
        Region a = null;
        if (ms.getData().get(0).size() > 1) {//check if this has boundary 
            a = new Region(ms.getAttribute(Const.NAME), ms.getData().get(0), ms.getCoordinate().getPossition());
            p = a.findMidCordinate();
        } else {
            p = ms.getCoordinate().getPossition();
        }
        try {
            dis = searchDistrict(p);
            ms.addAttribute(Const.REGION, dis.getName());

            if (a != null) {
                addToDistricPlacessWithBorders(ms, a);
            } else {
                addToDistrictPlaces(ms);
            }

        } catch (Exception e) {
            System.out.println("Can not find region for :" + ms.getAttribute(Const.NAME));
        }
    }

    private Region searchDistrict(double[] pont) {
        return districts.values().stream().filter((d) -> (d.isInside(pont))).findAny().get();
    }

    /**
     * this will find the best matching place for given point. it will
     * recursively check inside places with borders until it find most suitable
     * place.
     *
     * @param region the region that suspect point is located.
     * @param p position of point want to locate.
     * @return name of the place where most suitable.
     */
    private String findRelavantCityOrSuburb(Region region, double[] p) {
        String place;
        if (region.placesWithBordes.isEmpty() && region.places.isEmpty()) {
            return region.getName();
        }
        if (region.placesWithBordes.size() > 0) {
            try {
                Region city = region.placesWithBordes.entrySet().stream().filter((entry -> entry.getValue().isInside(p))).findAny().get().getValue();
                place = findRelavantCityOrSuburb(city, p);
            } catch (Exception e) {
                place = region.findNearestPlace(p);
            }
        } else {
            place = region.findNearestPlace(p);
        }
        return place;
    }

    private void groupSuburbsWithCities() {
        this.districts.entrySet().stream().forEach((e) -> {//creating adess like City- Suburb  eg  Issadeen town Matara town
            Region d = e.getValue();
            if (!(d.placesWithBordes.isEmpty() || d.cities.isEmpty() || d.suburbs.isEmpty())) {

                d.cities.stream().forEach((c) -> {
                    d.suburbs.stream().forEach((s) -> {

                        Region a = d.placesWithBordes.get(s.getCoordinate());
                        Point p = null;

                        double mid[] = null;
                        if (a != null) {
                            mid = a.findMidCordinate();
                        } else {
                            p = d.places.get(s.getCoordinate());
                            if (p != null) {
                                mid = p.getLocation();
                            }
                        }

                        if (mid != null) {

                            if (d.placesWithBordes.get(c.getCoordinate()).isInside(mid)) {

                                if (a != null) {
                                    a.setName(createCombinedName(c, s));
                                    d.placesWithBordes.get(c.getCoordinate()).placesWithBordes.put(s.getCoordinate(), a);
                                    d.placesWithBordes.remove(s.getCoordinate());
                                } else {
                                    p.setName(createCombinedName(c, s));
                                    d.placesWithBordes.get(c.getCoordinate()).places.put(s.getCoordinate(), p);
                                    d.places.remove(s.getCoordinate());
                                }

                                s.addAttribute(Const.NAME, createCombinedName(c, s));
                            }
                        }
                    });
                });
            }
            d.suburbs = null;
            d.cities = null;
        });
    }

    private String createCombinedName(MPSection city, MPSection suburb) {
        return suburb.getAttribute(Const.NAME) + "-" + city.getAttribute(Const.NAME);
    }

    /**
     * representation of large ares which contain serval small areas.
     */
    class Region extends Area {

        private Map<Coordinate, Point> places;
        private Map<Coordinate, Region> placesWithBordes;
        List<String> geoHash;
        List<MPSection> suburbs;
        List<MPSection> cities;

        public Region(String name, List<Coordinate> boundary, double[] possition) {
            super(name, boundary, possition);
            this.geoHash = new java.util.ArrayList<>();
            this.suburbs = new java.util.ArrayList<>();
            this.cities = new java.util.ArrayList<>();
            places = new java.util.HashMap<>();
            placesWithBordes = new java.util.HashMap<>();
        }

        public String findNearestPlace(double[] postition) {

            double distance = 100000;
            String currPlace = null;
            double x, y, dis;

            for (java.util.Map.Entry<Coordinate, Point> e : places.entrySet()) {

                x = e.getValue().getLocation()[0] - postition[0];
                y = e.getValue().getLocation()[1] - postition[1];
                dis = x * x + y * y;
                if (dis < distance) {
                    distance = dis;
                    currPlace = e.getValue().getName();
                }
            }
            return currPlace;
        }

    }

    /**
     * basic area with mid point and border .
     */
    class Area extends Point {

        private final double[][] bound;
        private double maxLat = -200, minLat = 200, maxLon = -200, minLon = 200;

        public Area(String name, List<Coordinate> boundary, double[] possition) {
            super(name, possition);

            bound = new double[boundary.size()][];

            int i = -1;
            for (Coordinate c : boundary) {
                bound[++i] = c.getPossition();
                if (maxLat < bound[i][0]) {
                    maxLat = bound[i][0];
                }
                if (minLat > bound[i][0]) {
                    minLat = bound[i][0];
                }

                if (maxLon < bound[i][1]) {
                    maxLon = bound[i][1];
                }
                if (minLon > bound[i][1]) {
                    minLon = bound[i][1];
                }
            }
            setLocation(possition);

        }

        public boolean isInside(double[] possition) {
            return isInsideBounds(possition) && contains(possition);
        }

        private boolean isInsideBounds(double[] c) {
            return (this.maxLat > c[0] && this.minLat < c[0] && this.maxLon > c[1] && this.minLon < c[1]);
        }

        public boolean contains(double[] c) {
            int i;
            int j;
            boolean result = false;
            for (i = 0, j = bound.length - 1; i < bound.length; j = i++) {
                if ((bound[i][0] > c[0]) != (bound[j][0] > c[0])
                        && (c[1] < (bound[j][1] - bound[i][1]) * (c[0] - bound[i][0]) / (bound[j][0] - bound[i][0]) + bound[i][1])) {
                    result = !result;
                }
            }
            return result;
        }

        public double[] findMidCordinate() {
            if (getLocation() == null) {
                setLocation(calculateMidPoint());
            }
            return getLocation();
        }

        private double[] calculateMidPoint() {
            double lat = 0, lon = 0;
            for (double c[] : bound) {
                lat += c[0];
                lon += c[1];
            }
            return new double[]{lat / this.bound.length, lon / this.bound.length};
        }

    }

    /**
     * point basic element of addressing system contains name and gps coordinate
     */
    class Point {

        private String name;
        private double location[];

        public Point(String name, double[] location) {
            this.name = name;
            this.location = location;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double[] getLocation() {
            return location;
        }

        public void setLocation(double[] location) {
            this.location = location;
        }

    }

}
