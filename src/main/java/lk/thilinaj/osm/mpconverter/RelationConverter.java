/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter;

import lk.thilinaj.osm.parser.Coordinate;
import lk.thilinaj.osm.parser.Member;
import lk.thilinaj.osm.parser.Node;
import lk.thilinaj.osm.parser.OSM;
import lk.thilinaj.osm.parser.Relation;
import lk.thilinaj.osm.parser.Way;
import lk.thilinaj.osm.mpconverter.mp.MP;
import lk.thilinaj.osm.mpconverter.mp.MPRestrictAndSignSection;
import lk.thilinaj.osm.mpconverter.mp.MPSection;
import java.util.List;

/**
 *
 * @author Thilina Jayamini
 */
public class RelationConverter extends Converter {

    //static Rule tmp;
    public static void processRelation(OSM o, Rule rNode, Rule rWay, Rule rPolygon, MP mp) {
        o.getRelations().entrySet().stream().forEach((entry) -> {

            Relation r = entry.getValue();

            if (r.isValidRelation(o)) {

                switch (r.getType()) {

                    case "multipolygon":
                        processMultyPolygon(r, o, rNode, rWay, rPolygon, mp);
                        break;
                    case "boundary":
                        switch (r.getProperty("boundary")) {
                            case "administrative":
                                processAdminBoundary(r, o, rPolygon, mp);
                                break;
                            case "political"://TODO political bounds
                                break;
                            case "national_park":
                                processMultyPolygon(r, o, rNode, rWay, rPolygon, mp);
                                break;
                        }
                        break;
                    case "destination_sign":
                        if (Config.applySigns) {
                            processSign(r, o, mp);
                        }
                        break;
                    case "restriction":
                        if (Config.applyRestrictions) {
                            processRestriction(r, o, mp);
                        }
                        break;
                    //TODO street 

                }
            }

        });
        o.clearRelations();
    }

    private static void processMultyPolygon(Relation r, OSM o, Rule rNode, Rule rWay, Rule rPolygon, MP mp) {

        List<List<Coordinate>> boundary = getBoundary(r, o);
        if (boundary != null) {
            findRuleForRelation(r, o, rNode, rWay, rPolygon, mp, boundary);
        }

    }

    private static List<List<Coordinate>> getBoundary(Relation r, OSM o) {
        List<String> outerMembers = r.getMembersByRole("outer");
        if (outerMembers == null || outerMembers.isEmpty() || outerMembers.get(0).isEmpty()) {
            return null;
        }

        List<List<Coordinate>> boundary = mergeLine(o, outerMembers, r.getId());
        if (boundary.isEmpty() || boundary.get(0).isEmpty()) {
            return null;
        }
        List<String> innerMembers = r.getMembersByRole("inner");
        if (r.isMembersPresentWithType("relation", "inner")) {
            innerMembers = fixRelation(innerMembers, o);
        }
        boundary.addAll(mergeLine(o, innerMembers, r.getId()));
        return boundary;
    }

    private static List<String> fixRelation(List<String> members, OSM o) {
        List<String> tmpLst = new java.util.ArrayList<>(members);
        for (String s : members) {
            if (o.getWays().get(s) == null) {
                Relation get = o.getRelations().get(s);
                if (get != null) {
                    if (get.isValidRelation(o)) {
                        tmpLst = get.getMembersByRole("outer");
                        if (tmpLst == null || tmpLst.isEmpty()) {
                            tmpLst = get.getMembersByRole("inner");
                            if (tmpLst == null || tmpLst.isEmpty()) {
                                continue;
                            }
                        }
                        tmpLst.remove(s);
                        fixRelation(tmpLst, o);
                    }
                } else {
                    return new java.util.ArrayList<>();//if it come here menas member is not way or relation and canot process further.
                    //to avoid null point returning empty list
                }
            }
        }
        return tmpLst;
    }

    private static void findRuleForRelation(Relation r, OSM o, Rule rNode, Rule rWay, Rule rPolygon, MP mp, List<List<Coordinate>> boundary) {
        Rule tr = findeRule(r, rPolygon);
        if (tr != null) {
            mp.addPolygon(processElement(tr, r, o, boundary));
        } else {
            tr = findeRule(r, rNode);
            if (tr != null) {
                Node n = new Node(findMidCoordinate(boundary.get(0)));
                Rule ra = tr.data.equals("FU") ? new Rule(null, "0x0013", "0") : new Rule(null, tr.data.split(";")[0], tr.data.split(";")[1]);
                areaToPoint(n, r, tr, ra, boundary, o, mp);

            }
        }
    }

    private static void processAdminBoundary(Relation r, OSM o, Rule rPolygon, MP mp) {
        List<List<Coordinate>> boundary = getBoundary(r, o);
        if (boundary == null || boundary.isEmpty()) {
            return;
        }

        String admLvl = r.getProperty("admin_level");
        String name;
        switch (admLvl) {
            case "2":
                if (!r.getProperty("name:en").isEmpty()) {
                    registry.setCountry(r.getProperty("name:en"));
                } else {
                    registry.setCountry(r.getProperty("name"));
                }
                return;
            case "3":
            case "4":
            case "6":
            case "7":
            case "10":
                return;

            case "5":
                name = r.getProperty("name:en");
                if (name.isEmpty()) {
                    name = r.getProperty("name");
                }
                registry.addDistrict(name, boundary);
                return;
        }

        List<String> members = r.getMembersByRole("admin_centre");
        Coordinate p = null;
        if (members != null && !members.isEmpty()) {
            r.getTags().clear();
            Node n = o.getNodes().get(members.get(0));
            if (n == null) {
                printError("Error : Please check relation " + r.getId());
                return;
            }
            r.getTags().putAll(n.getTags());
            p = n.getCordinate();
        }
        Rule tr = findeRule(r, rPolygon);
        if (tr != null) {
            MPSection ms = processElement(tr, r, o, boundary);
            if (p != null) {
                ms.setAdminCentre(p);
            }
            mp.addPolygon(ms);
        }
    }

    private static void processRestriction(Relation r, OSM o, MP mp) {
        List<Member> mbrs = r.getMembers();
        if (mbrs.isEmpty() || mbrs.size() < 3) {
            printError("Error: restriction " + r.getId() + " is incomplete");
            return;
        }
        MPRestrictAndSignSection ms = new MPRestrictAndSignSection();
        ms.setType(r.getTags().get("restriction"));
        mbrs.stream().forEach((m) -> {
            switch (m.getRole()) {
                case "from":
                    ms.setFromWay(m.getRef());
                    break;
                case "to":
                    ms.setToWay(m.getRef());
                    break;
                case "via":
                    if (m.getType().equals("node")) {
                        ms.addVia(m.getRef());
                    } else {
                        Way w = o.getWays().get(m.getRef());
                        ms.setVia(w.getNodes());
                        ms.setViaWay(w.getId());
                    }
                    break;
            }
        });
        if (isInvalidRestriction(ms, o)) {
            printError("Error: restriction " + r.getId() + " is incorrect or unwanted. check error message above");
            return;
        }
        parseTagsForRestrictAndSign(r, ms);
        mp.addRestrict(ms);

    }

    private static void processSign(Relation r, OSM o, MP mp) {
        List<Member> mbrs = r.getMembers();
        if (mbrs.isEmpty() || mbrs.size() < 3) {
            printError("Error: Sign " + r.getId() + "is incomplete");
            return;
        }
        MPRestrictAndSignSection ms = new MPRestrictAndSignSection();
        mbrs.stream().forEach((m) -> {
            switch (m.getRole()) {
                case "from":
                    ms.setFromWay(m.getRef());
                    break;
                case "to":
                    ms.setToWay(m.getRef());
                    break;
                case "sign":
                    ms.addVia(m.getRef());
                    break;

            }
        });

        parseTags(r, ms);
        mp.addSign(ms);

    }

    //[Emergency],[delivery],[car],[bus],[taxi],[pedestrian],[bicycle],[truck] 
    private static void parseTagsForRestrictAndSign(Relation r, MPRestrictAndSignSection ms) {
        char[] p = new char[]{'0', '0', '0', '0', '0', '1', '0', '0'};
        r.getTags().entrySet().stream().forEach((s) -> {

            if (s.getValue() != null) {

                switch (s.getKey()) {
                    case "except":
                        String[] arr = s.getValue().split(";");
                        for (String srt : arr) {
                            switch (srt) {
                                case "motorcar":
                                    p[2] = '1';
                                    break;
                                case "emergency":
                                    p[0] = '1';
                                    break;
                                case "psv":
                                    p[3] = '1';
                                    p[4] = '1';
                                    break;
                                case "bicycle":
                                    p[6] = '1';
                                    break;
                            }
                        }
                        break;
                    //TODO parse remaining tags 
                        /*
                     except 	psv / bicycle / hgv / motorcar / emergency 	The restriction does not apply to these vehicle types (more than one: except=bicycle;psv)
                     day_on 	day of week 	for example, no right turn in the morning peak on weekdays might be day_on=Monday;day_off=Friday;hour_on=07:30;hour_off=09:30
                     day_off 	day of week 	
                     hour_on 	time of day 	
                     hour_off 	time of day
                     */

                }
            }
            ms.addComment(s.getKey() + "=" + s.getValue());
        });
        ms.addComment("relation:" + r.getId());
        ms.addAttribute("RestrParam", java.text.MessageFormat.format("{0},{1},{2},{3},{4},{5},{6},{7}", p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]));
    }

    private static boolean isInvalidRestriction(MPRestrictAndSignSection ms, OSM o) {

        String viaFirst = o.getNodes().get(ms.getVia().get(0)).getId();
        String viaLast = o.getNodes().get(getLast(ms.getVia())).getId();

        Way wf = o.getWays().get(ms.getFromWay());
        Way wt = o.getWays().get(ms.getToWay());

        String wfFirst = wf.getNodes().get(0);
        String wfLast = getLast(wf.getNodes());

        String wtFirst = wt.getNodes().get(0);
        String wtLast = getLast(wt.getNodes());
        if (wf.getId().equals(wt.getId()) && ms.getType().equals(Const.RESTRICT_NO_U_TURN)) {
            printError("Unwanted u turn. From way and to way pointed to same way. Way" + wf.getId() + " via " + viaFirst);
            return true;
        }
        /*no need to find index any more fill restriction in road converter modifyied 
         int result = validateFirstAndLast(wtFirst, wtLast, viaFirst, viaLast);*/
        if (validateFirstAndLast(wtFirst, wtLast, viaFirst, viaLast)) {
            //ms.setToWayIdx(result);
        } else {
            printError("To_Way is not go through via");
            return true;
        }
        /* no need to find index any more fill restriction in road converter modifyied 
         result = validateFirstAndLast(wfFirst, wfLast, viaFirst, viaLast, wf.length());*/
        if (validateFirstAndLast(wfFirst, wfLast, viaFirst, viaLast)) {
            //ms.setFromWayIdx(result);
        } else {
            printError("From_Way is not go through via");
            return true;
        }

        return false;
    }

    private static boolean validateFirstAndLast(String wayFirst, String wayLast, String viaFirst, String viaLast) {

        return wayFirst.equals(viaFirst) || wayFirst.equals(viaLast) || wayLast.equals(viaFirst) || wayLast.equals(viaLast);

    }

}
