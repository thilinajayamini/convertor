/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import lk.thilinaj.osm.parser.Coordinate;

/**
 *
 * @author Thilina Jayamini - thilinajayamini@gmail.com
 */
public class PolyFileHandler {

    public static List<Coordinate> read(String fileName) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        String line;
        String[] arr = null;
        String lat = null, lon;

        List<Coordinate> bound = new ArrayList<>();

        while (true) {
            line = br.readLine().trim();
            lon = null;
            if (line.length() > 6) {
                arr = line.split(" ");

                for (String arr1 : arr) {
                    if (arr1.length() > 1) {
                        if (lon == null) {
                            lon = arr1;
                        } else {
                            lat = arr1;
                        }
                    }
                }

                bound.add(new Coordinate(lat, lon));

            } else {
                if (arr != null) {
                    break;
                }
            }

        }

        Converter.printInfo(bound.size() + " segments loaded as boundary from poly file");
        return bound;

    }

    public static void write(String fileNmae) {

    }

}
