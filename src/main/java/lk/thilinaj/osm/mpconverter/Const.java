/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class Const {

    public static final String HOUSE_NO = "HouseNumber";
    public static final String STREET = "StreetDesc";
    public static final String CITY = "CityName";
    public static final String SUBURB = "Suburb";
    public static final String REGION = "RegionName";
    public static final String DISTRICT = "District";
    public static final String COUNTRY = "CountryName";
    public static final String ZIP = "Zip";
    public static final String IS_A_CITY = "City";

    public static final String NAME = "Label";
    public static final String TYPE = "Type";
    public static final String E_L = "EndLevel";
    
    public static final String RESTRICT_NO_U_TURN = "no_u_turn";

}
