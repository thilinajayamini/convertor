/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter;

import java.io.FileReader;
import java.text.DecimalFormat;
import lk.thilinaj.osm.mpconverter.mp.MPWriter;
import lk.thilinaj.osm.parser.Coordinate;
import lk.thilinaj.osm.parser.Element;
import lk.thilinaj.osm.parser.OSM;
import lk.thilinaj.osm.parser.Node;
import lk.thilinaj.osm.parser.Relation;
import lk.thilinaj.osm.parser.Way;
import lk.thilinaj.osm.mpconverter.mp.MP;
import lk.thilinaj.osm.mpconverter.mp.MPSection;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import lk.thilinaj.osm.parser.OSMLoader;

public class Converter {

    static AddressRegistry registry = new AddressRegistry();
    static long period = 0, start = 0, totoalTime = 0;
    protected static MPWriter writer;

    public static void main(String[] args) throws Exception {
        String settings;
        if (args.length > 0) {
            settings = args[0];
        } else {
            System.err.println("Usage: java -jar osm2mp-java.jar <settings_file>");
            System.err.println("No settings file given useing default settings");
            settings = "default.cfg";
        }

        RuleSet rSet = loadConfig(settings);

        OSM o = OSMLoader.load(Config.inputFile + ".osm");

        printStatus("OSM loading done : ");
        MP mp = new MP(Config.imgHeader);

        RelationConverter.processRelation(o, rSet.rNode, rSet.rWay, rSet.rPolygon, mp);
        printStatus("Relations done : ");

        createPOI(o, rSet.rNode, mp);
        printStatus("Nodes done : ");

        createPoligonsAndLines(o, rSet.rNode, rSet.rWay, rSet.rPolygon, mp);
        printStatus("Ways done : ");

        registry.build();
        printStatus("Registry Built : ");

        if (Config.addressing) {
            mp.fillAddress(registry);
            printStatus("Address Resolved : ");
        }

        createCoastLineandSea(o, mp);
        createBackground(o, mp);

        RoadConverter.convertRoads(o, mp, rSet.rRoad);
        printStatus("Roads done : ");

        mp.writeToFile(writer);//TODO start printing in separate thread polygons 

        printStatus("Print done : ");
        writer.close();

    }

    private static RuleSet loadConfig(String settingsFile) throws Exception {
        loadSettings(settingsFile);
        writer = new MPWriter(Config.inputFile, Config.outputFile);
        start = System.currentTimeMillis();
        RuleSet rSet = new RuleSet();

        printStatus("Loading Rules....");

        rSet.rNode = RuleLoader.loadRules(null, Config.rNode);
        rSet.rWay = RuleLoader.loadRules(null, Config.rLine);
        rSet.rPolygon = RuleLoader.loadRules(null, Config.rPolygon);
        rSet.rRoad = RuleLoader.loadRules(null, Config.rRoad);
        if (rSet.rNode == null || rSet.rWay == null || rSet.rPolygon == null || rSet.rRoad == null) {
            throw new Exception("Rule loading Error");
        }
        printStatus("for rule loading: ");

        return rSet;
    }

    private static void loadSettings(String settingsFile) throws Exception {
        Properties p = new Properties();
        p.load(new FileReader(settingsFile));

        Config.mapName = p.getProperty("mapName");
        Config.mapCode = p.getProperty("mapCode");
        Config.inputFile = p.getProperty("inputFile");
        Config.outputFile = p.getProperty("outputFile");
        Config.polyFile = p.getProperty("polyFile");
        Config.imgHeader = p.getProperty("imgHeader", "img_head.txt");

        Config.addressing = Boolean.parseBoolean(p.getProperty("addressing"));
        Config.seaBackGround = Boolean.parseBoolean(p.getProperty("seaBackGround"));
        Config.enableLayes = Boolean.parseBoolean(p.getProperty("enableLayes"));
        Config.splitRoads = Boolean.parseBoolean(p.getProperty("splitRoads"));
        Config.fixSelfIntersection = Boolean.parseBoolean(p.getProperty("fixSelfIntersection"));
        Config.applyRestrictions = Boolean.parseBoolean(p.getProperty("applyRestrictions"));
        Config.applySigns = Boolean.parseBoolean(p.getProperty("applySigns"));

        Config.maxRoadNodes = Integer.parseInt(p.getProperty("maxRoadNodes"));

        Config.rNode = p.getProperty("nodes", "nodes.csv");
        Config.rLine = p.getProperty("lines", "lines.csv");
        Config.rPolygon = p.getProperty("polygons", "polygons.csv");
        Config.rRoad = p.getProperty("roads", "roads.csv");

    }

    private static void printStatus(String msg) {
        period = (System.currentTimeMillis() - start) - totoalTime;
        totoalTime = System.currentTimeMillis() - start;
        printInfo(msg + "this section: " + period + " till now total: " + totoalTime);
    }

    protected static void printInfo(String msg) {
        writer.printInfo(msg);
    }

    protected static void printError(String msg) {
        writer.printError(msg);
    }

    private static void createCoastLineandSea(OSM o, MP mp) {

        List<String> coastalLine = o.getWays().entrySet().parallelStream().
                filter((t) -> t.getValue().getTags().entrySet().stream().
                        anyMatch((m) -> (m.getKey().equals("natural") && m.getValue().equals("coastline")))).
                map((t) -> (t.getValue().getId())).collect(Collectors.toList());

        Rule tr = new Rule(null, "0x15", "6");

        coastalLine.stream().forEach((s) -> {//TODO process cost line with name tag (Eg Sri lanka, kachhcathiwe, delft)
            o.getWays().get(s).getTags().clear();
            mp.addPolyLine(processElement(tr, o.getWays().get(s), o));
        });

        printStatus("Coast Line done : ");

        if (Config.seaBackGround) {
            mp.addPolygon(cretateSeaPolygon(o, coastalLine, Config.polyFile));
            printStatus("Sea done : ");
        }
    }

    private static void createPOI(OSM o, Rule r, MP mp) {
        Node n;
        Rule tr;
        for (String key : o.getNodes().keySet()) {

            n = o.getNodes().get(key);
            tr = findeRule(n, r);
            if (tr != null) {
                mp.addPOI(processElement(tr, n, o));
            }
        }
    }

    private static void createPoligonsAndLines(OSM o, Rule rNode, Rule rWay, Rule rPolygon, MP mp) {

        Way w;
        Rule tr;
        for (String key : o.getWays().keySet()) {
            w = o.getWays().get(key);
            if (w.isClosed()) {
                tr = findeRule(w, rPolygon);
                if (tr != null) {
                    mp.addPolygon(processElement(tr, w, o));
                } else {
                    tr = findeRule(w, rNode);
                    if (tr != null) {
                        Node n = findMidPoint(w.getNodes(), o);
                        Rule ra = tr.data.equals("FU") ? new Rule(null, "0x0013", "0") : new Rule(null, tr.data.split(";")[0], tr.data.split(";")[1]);
                        areaToPoint(n, w, tr, ra, null, o, mp);

                        continue;
                    }
                    if (findMinorTag(w.getTags())) {
                        mp.addPolygon(processElement(new Rule(null, "0x0013", "0"), w, o));
                    }
                }
            } else {
                tr = findeRule(w, rWay);
                if (tr != null) {
                    mp.addPolyLine(processElement(tr, w, o));
                }
            }
        }
        System.gc();
    }

    /**
     * convert area element to point.
     *
     * @param n node that going to represent the area. (this nod will show on
     * map with properties of given area)
     * @param e area element.
     * @param ra rule to convert point.
     * @param rp rule to convert area.
     * @param data boundary for multi polygon.
     * @param o OSM data.
     * @param mp MP data.
     */
    protected static void areaToPoint(Node n, Element e, Rule rp, Rule ra, List<List<Coordinate>> data, OSM o, MP mp) {        
        n.getTags().putAll(e.getTags());
        n.getTags().put("origin from " + e.getClass().getSimpleName(), e.getId());
        mp.addPOI(processElement(rp, n, o));
        if (e.getTags().get("natural") == null || !e.getTags().get("natural").equals("coastline")) {//this is not closed way island
            if (ra.code.equals("0x0013") && e.getTags().get("building") == null) {
                return;
            }
            e.getTags().clear();
            mp.addPolygon(processElement(ra, e, o, data));
        }

    }

    protected static Rule findeRule(Element e, Rule r) {
        String k;
        Rule tmp;
        for (java.util.Map.Entry<String, String> entry : e.getTags().entrySet()) {

            k = entry.getKey() + "=" + entry.getValue();
            tmp = r.subSet.get(k);
            if (tmp == null) {
                continue;
            }

            if (tmp.subSet.isEmpty()) {

                return tmp;

            } else {
                r = tmp;
            }
        }
        return r != null && r.code != null ? r : null;
    }

    protected static MPSection processElement(Rule r, Element e, OSM o, List<List<Coordinate>> data) {
        MPSection ms = new MPSection();
        parseTags(e, ms);
        ms.addAttribute(Const.TYPE, r.code);
        ms.addAttribute(Const.E_L, r.endLevel);

        switch (e.getClass().getSimpleName()) {
            case "Node":
                ms.addComment("type:node");
                ms.addComment("id:" + e.getId());
                ms.addCordinate(((Node) e).getCordinate());
                break;
            case "Way":
                ms.addComment("type:way");
                ms.addComment("id:" + e.getId());
                ((Way) e).getNodes().forEach((String s) -> {
                    ms.addCordinate(o.getNodes().get(s).getCordinate());
                });
                break;
            case "Relation":
                ms.addComment("type:relation");
                ms.addComment("id:" + e.getId());
                data.stream().forEach((d) -> {
                    ms.addDataList(d);
                });
                break;

        }
        if (ms.isPlace()) {
            registry.addPlace(ms);
            ms.deleteAttribute(Const.CITY);
        }

        return ms;
    }

    protected static void parseTags(Element e, MPSection ms) {
        e.getTags().entrySet().stream().forEach((s) -> {

            if (s.getValue() != null) {

                switch (s.getKey()) {
                    case "name":
                        ms.addAttribute(Const.NAME, s.getValue());
                        break;
                    case "name:en":
                        if (e instanceof Relation) {
                            ms.addAttribute(Const.NAME, s.getValue());
                        }
                        break;
                    case "addr:housenumber":
                        ms.addAttribute(Const.HOUSE_NO, s.getValue());
                        ms.markAsValidAddress();
                        break;
                    case "addr:housename":
                        ms.addAttribute(Const.NAME, s.getValue());
                        ms.markAsValidAddress();
                        break;
                    case "addr:street":
                        ms.addAttribute(Const.STREET, s.getValue());
                        ms.markAsValidAddress();
                        break;
                    case "addr:suburb":
                        ms.addAttribute(Const.SUBURB, s.getValue());
                        ms.markAsValidAddress();
                        break;
                    case "addr:city":
                    case "is_in:city":
                        ms.addAttribute(Const.CITY, s.getValue());
                        ms.markAsValidAddress();
                        break;
                    case "addr:region":
                    case "is_in:district":
                        ms.addAttribute(Const.REGION, s.getValue());
                        break;
                    case "addr:state":
                    case "is_in:state":
                        ms.addAttribute("State", s.getValue());
                        break;
                    case "addr:country":
                    case "is_in:country":
                        ms.addAttribute(Const.COUNTRY, s.getValue());
                        ms.markAsValidAddress();
                        break;
                    case "is_in:province":
                        ms.addAttribute("Province", s.getValue());
                        break;
                    case "addr:postcode":
                        ms.addAttribute(Const.ZIP, s.getValue());
                        break;
                    case "website":
                    case "url":
                        ms.addAttribute("WebPage", s.getValue());
                        break;
                    case "phone":
                        ms.addAttribute("Phone", s.getValue());
                        break;
                    case "opening_hours":
                        ms.addAttribute("OpeningHours", s.getValue());
                        break;
                    case "description":
                        ms.addAttribute("Text", s.getValue());
                        break;
                    case "population":
                        ms.addAttribute("Population", s.getValue());
                        break;
                    case "admin_level":
                        ms.addAttribute("AdminLevel", s.getValue());
                        break;
                    case "operator":
                        ms.addAttribute("Operator", s.getValue());
                        break;
                    case "building:levels":
                        ms.addAttribute("Floors", s.getValue());
                        break;
                    case "destination":
                        switch (e.getClass().getSimpleName()) {
                            case "Relation"://destination_sign relation
                                ms.addAttribute("SignParam", "T," + s.getValue());//type is T , Toward
                                break;
                            case "Node": //Motor way junction (node)
                                ms.addAttribute(Const.NAME, s.getValue());
                                break;
                            case "Way"://high way link or highway 
                                if (e.getTags().get("highway").endsWith("_link")) {//
                                    ms.addAttribute(Const.NAME, s.getValue());
                                } else {
                                    ms.addAttribute("Destination", s.getValue());
                                }
                                break;

                        }
                        break;
                    case "place":
                        if (s.getValue().equals("city") || s.getValue().equals("suburb") || s.getValue().equals("town") || s.getValue().equals("village") || s.getValue().equals("hamlet")) {
                            ms.markAsPlace();
                        }
                        break;
                }

            } else {
                printInfo(e.getId());
            }
            ms.addComment(s.getKey() + "=" + s.getValue());

        });
    }

    protected static MPSection processElement(Rule r, Element e, OSM o) {
        return processElement(r, e, o, null);
    }

    /**
     * create sea polygon from coast line and boundary (pass null if not have
     * poly file)
     *
     * @param o OSM
     * @param polyFile Boundary Polygon
     * @return
     */
    private static MPSection cretateSeaPolygon(OSM o, List<String> coastalLine, String polyFile) {
        java.util.List<Coordinate> bounds = getMapBounds(o, polyFile);

        MPSection ms = new MPSection();

        ms.addComment("sea backgroud");
        ms.addAttribute(Const.TYPE, "0x28");
        ms.addAttribute(Const.E_L, "6");

        ms.addDataList(bounds);
        mergeLine(o, coastalLine, "CoastalLine").stream().forEach((s) -> {
            ms.addDataList(s);
        });

        return ms;

    }

    private static void createBackground(OSM o, MP mp) {
        java.util.List<Coordinate> bounds = getMapBounds(o, Config.polyFile);
        MPSection ms = new MPSection();

        ms.addComment("backgroud");
        ms.addAttribute(Const.TYPE, "0x4B");
        ms.addDataList(bounds);
        mp.addPolygon(ms);
    }

    private static java.util.List<Coordinate> getMapBounds(OSM o, String polyFile) {
        java.util.List<Coordinate> bounds;
        if (polyFile == null) {
            bounds = o.getBounds();
        } else {
            try {
                bounds = loadPolyFile(polyFile);
            } catch (Exception ex) {
                printError(ex.getMessage());
                bounds = o.getBounds();
            }
        }
        return bounds;
    }

    private static java.util.List<Coordinate> loadPolyFile(String polyFile) throws Exception {
        return PolyFileHandler.read(polyFile);
    }

    protected static java.util.List<java.util.List<Coordinate>> mergeLine(OSM o, java.util.List<String> line, String elementId) {
        java.util.List<java.util.List<Coordinate>> closedPolygons = new java.util.ArrayList<>();
        java.util.List<java.util.List<String>> openWays = new java.util.ArrayList<>();

        line.stream().forEach((s) -> {
            Way w = o.getWays().get(s);
            if (w != null) {
                if (w.isClosed()) {
                    closedPolygons.add(new java.util.ArrayList<>(getNodesAsCoordinates(o, w.getNodes())));
                } else {
                    openWays.add(w.getNodes());
                }
            }
        });
        int x = 0;
        int size;

        java.util.List<String> temp = new java.util.ArrayList<>();
        if (!openWays.isEmpty()) {
            temp.addAll(openWays.get(0));
        }

        while (!openWays.isEmpty()) {

            String lastNode = getLast(temp);//get the last node in first node list
            size = temp.size();

            for (int i = 1; i < openWays.size(); i++) {// find the list that contain first node which match with 

                if (getLast(openWays.get(i)).equals(lastNode)) {//now elements are not in same way need to reverse to combine
                    java.util.Collections.reverse(openWays.get(i));
                }
                if (openWays.get(i).get(0).equals(lastNode)) {
                    temp.remove(size - 1);//avoid duplicate node when merging
                    temp.addAll(openWays.get(i));
                    openWays.remove(openWays.get(i));//since it merged no need to keep

                    if (temp.get(0).equals(getLast(temp))) {//after merging check the way is closed.   
                        closedPolygons.add(new java.util.ArrayList<>(getNodesAsCoordinates(o, temp)));
                        openWays.remove(0);//remove fist one which is temp
                        if (!openWays.isEmpty()) {
                            temp = new java.util.ArrayList<>();
                            temp.addAll(openWays.get(0));
                        }
                    }
                    break;
                }
            }

            if (!openWays.isEmpty() && size == temp.size()) {//this section cannot ableto find another joining way and its not close. so this is break. need to remove
                printError("Relation  " + elementId + " has unclosed way ");
                openWays.remove(0);
                if (!openWays.isEmpty()) {
                    temp = new java.util.ArrayList<>();
                    temp.addAll(openWays.get(0));
                }
            }

            if (x == 100) {
                printError("there is line breake in relation " + elementId);//TODO improve for partial sea polygons
                break;
            }
        }
        return closedPolygons;
    }

    protected static java.util.List<Coordinate> getNodesAsCoordinates(OSM o, java.util.List<String> nodes) {
        java.util.List<Coordinate> clst = new java.util.ArrayList<>();
        nodes.stream().forEach((n) -> {
            clst.add(o.getNodes().get(n).getCordinate());
        });
        return clst;
    }

    protected static Node findMidPoint(java.util.List<String> nodes, OSM o) {
        Coordinate midC = findMidCoordinate(nodes, o);
        return new Node(midC);
    }

    protected static Coordinate findMidCoordinate(java.util.List<String> nodes, OSM o) {
        return findMidCoordinate(getNodesAsCoordinates(o, nodes));
    }

    protected static Coordinate findMidCoordinate(List<Coordinate> outer) {
        double lat = 0;
        double lon = 0;
        DecimalFormat fm = new DecimalFormat("###.######");
        for (Coordinate c : outer) {
            lat += Double.parseDouble(c.getLat());
            lon += Double.parseDouble(c.getLon());
        }
        return new Coordinate(fm.format(lat / outer.size()), fm.format(lon / outer.size()));
    }

    protected static boolean findMinorTag(java.util.Map<String, String> tags) {
        return tags.entrySet().stream().anyMatch((t) -> (t.getKey().equals("building") || t.getKey().equals("addr")));
    }

    protected static <T extends Object> T getLast(java.util.List<T> lst) {
        return getByIndex(lst, -1);
    }

    /**
     * get by list index. (-) values for access from last.
     *
     * @param <T>
     * @param lst
     * @param index
     * @return
     */
    protected static <T extends Object> T getByIndex(java.util.List<T> lst, int index) {
        if (index > 0) {
            return lst.get(index);
        }
        return lst.get(lst.size() + index);
    }

    protected static String getColourCode(String strColour) {
        switch (strColour) {
            case "aliceblue":
                return "#FAEBD7";
            case "antiquewhite":
                return "#FAEBD7";
            case "aqua":
                return "#00FFFF";
            case "aquamarine":
                return "#7FFFD4";
            case "azure":
                return "#F0FFFF";
            case "beige":
                return "#F5F5DC";
            case "bisque":
                return "#FFE4C4";
            case "black":
                return "#000000";
            case "blanchedalmond":
                return "#FFEBCD";
            case "blue":
                return "#0000FF";
            case "blueviolet":
                return "#8A2BE2";
            case "brown":
                return "#A52A2A";
            case "burlywood":
                return "#DEB887";
            case "cadetblue":
                return "#5F9EA0";
            case "chartreuse":
                return "#7FFF00";
            case "chocolate":
                return "#D2691E";
            case "coral":
                return "#FF7F50";
            case "cornflowerblue":
                return "#6495ED";
            case "cornsilk":
                return "#FFF8DC";
            case "crimson":
                return "#DC143C";
            case "cyan":
                return "#00FFFF";
            case "darkblue":
                return "#00008B";
            case "darkcyan":
                return "#008B8B";
            case "darkgoldenrod":
                return "#B8860B";
            case "darkgray":
                return "#A9A9A9";
            case "darkgrey":
                return "#A9A9A9";
            case "darkgreen":
                return "#006400";
            case "darkkhaki":
                return "#BDB76B";
            case "darkmagenta":
                return "#8B008B";
            case "darkolivegreen":
                return "#556B2F";
            case "darkorange":
                return "#FF8C00";
            case "darkorchid":
                return "#9932CC";
            case "darkred":
                return "#8B0000";
            case "darksalmon":
                return "#E9967A";
            case "darkseagreen":
                return "#8FBC8F";
            case "darkslateblue":
                return "#483D8B";
            case "darkslategray":
                return "#2F4F4F";
            case "darkslategrey":
                return "#2F4F4F";
            case "darkturquoise":
                return "#00CED1";
            case "darkviolet":
                return "#9400D3";
            case "deeppink":
                return "#FF1493";
            case "deepskyblue":
                return "#00BFFF";
            case "dimgray":
                return "#696969";
            case "dimgrey":
                return "#696969";
            case "dodgerblue":
                return "#1E90FF";
            case "firebrick":
                return "#B22222";
            case "floralwhite":
                return "#FFFAF0";
            case "forestgreen":
                return "#228B22";
            case "fuchsia":
                return "#FF00FF";
            case "gainsboro":
                return "#DCDCDC";
            case "ghostwhite":
                return "#F8F8FF";
            case "gold":
                return "#FFD700";
            case "goldenrod":
                return "#DAA520";
            case "gray":
                return "#808080";
            case "grey":
                return "#808080";
            case "green":
                return "#008000";
            case "greenyellow":
                return "#ADFF2F";
            case "honeydew":
                return "#F0FFF0";
            case "hotpink":
                return "#FF69B4";
            case "indianred":
                return "#CD5C5C";
            case "indigo":
                return "#4B0082";
            case "ivory":
                return "#FFFFF0";
            case "khaki":
                return "#F0E68C";
            case "lavender":
                return "#E6E6FA";
            case "lavenderblush":
                return "#FFF0F5";
            case "lawngreen":
                return "#7CFC00";
            case "lemonchiffon":
                return "#FFFACD";
            case "lightblue":
                return "#ADD8E6";
            case "lightcoral":
                return "#F08080";
            case "lightcyan":
                return "#E0FFFF";
            case "lightgoldenrodyellow":
                return "#FAFAD2";
            case "lightgray":
                return "#D3D3D3";
            case "lightgrey":
                return "#D3D3D3";
            case "lightgreen":
                return "#90EE90";
            case "lightpink":
                return "#FFB6C1";
            case "lightsalmon":
                return "#FFA07A";
            case "lightseagreen":
                return "#20B2AA";
            case "lightskyblue":
                return "#87CEFA";
            case "lightslategray":
                return "#778899";
            case "lightslategrey":
                return "#778999";
            case "lightsteelblue":
                return "#B0C4DE";
            case "lightyellow":
                return "#FFFFE0";
            case "lime":
                return "#00FF00";
            case "limegreen":
                return "#32CD32";
            case "linen":
                return "#FAF0E6";
            case "magenta":
                return "#FF00FF";
            case "maroon":
                return "#800000";
            case "mediumaquamarine":
                return "#66CDAA";
            case "mediumblue":
                return "#0000CD";
            case "mediumorchid":
                return "#BA55D3";
            case "mediumpurple":
                return "#9370D8";
            case "mediumseagreen":
                return "#3CB371";
            case "mediumslateblue":
                return "#7B68EE";
            case "mediumspringgreen":
                return "#00FA9A";
            case "mediumturquoise":
                return "#48D1CC";
            case "mediumvioletred":
                return "#C71585";
            case "midnightblue":
                return "#191970";
            case "mintcream":
                return "#F5FFFA";
            case "mistyrose":
                return "#FFE4E1";
            case "moccasin":
                return "#FFE4B5";
            case "navajowhite":
                return "#FFDEAD";
            case "navy":
                return "#000080";
            case "oldlace":
                return "#FDF5E6";
            case "olive":
                return "#808000";
            case "olivedrab":
                return "#6B8E23";
            case "orange":
                return "#FFA500";
            case "orangered":
                return "#FF4500";
            case "orchid":
                return "#DA70D6";
            case "palegoldenrod":
                return "#EEE8AA";
            case "palegreen":
                return "#98FB98";
            case "paleturquoise":
                return "#AFEEEE";
            case "palevioletred":
                return "#D87093";
            case "papayawhip":
                return "#FFEFD5";
            case "peachpuff":
                return "#FFDAB9";
            case "peru":
                return "#CD853F";
            case "pink":
                return "#FFC0CB";
            case "plum":
                return "#DDA0DD";
            case "powderblue":
                return "#B0E0E6";
            case "purple":
                return "#800080";
            case "red":
                return "#FF0000";
            case "rosybrown":
                return "#BC8F8F";
            case "royalblue":
                return "#4169E1";
            case "saddlebrown":
                return "#8B4513";
            case "salmon":
                return "#FA8072";
            case "sandybrown":
                return "#F4A460";
            case "seagreen":
                return "#2E8B57";
            case "seashell":
                return "#FFF5EE";
            case "sienna":
                return "#A0522D";
            case "silver":
                return "#C0C0C0";
            case "skyblue":
                return "#87CEEB";
            case "slateblue":
                return "#6A5ACD";
            case "slategray":
                return "#708090";
            case "slategrey":
                return "#708090";
            case "snow":
                return "#FFFAFA";
            case "springgreen":
                return "#00FF7F";
            case "steelblue":
                return "#4682B4";
            case "tan":
                return "#D2B48C";
            case "teal":
                return "#008080";
            case "thistle":
                return "#D8BFD8";
            case "tomato":
                return "#FF6347";
            case "turquoise":
                return "#40E0D0";
            case "violet":
                return "#EE82EE";
            case "wheat":
                return "#F5DEB3";
            case "white":
                return "#FFFFFF";
            case "whitesmoke":
                return "#F5F5F5";
            case "yellow":
                return "#FFFF00";
            case "yellowgreen":
                return "#9ACD32";
            default:
                return "#FFFFFF";
        }
    }

}

/*
 private static MPSection processWay(Rule r, Way e, OSM o) {
 MPSection ms = new MPSection();
 ms.addComment(e.getId());
 e.getTags().stream().forEach((s) -> {
 switch (s.split("=")[0]) {
 case "name":
 ms.addAttribute("Label=" + s.split("=")[1]);
 break;
 }
 ms.addComment(s);

 });
 ms.addAttribute("Type=" + r.code);
 ms.addAttribute("EndLevel=" + r.endLevel);

 e.getNodes().forEach((String s) -> {
 ms.addCordinate(o.getNodes().get(s).getCordinate());
 });

 return ms;

 }*/
/*
 private static MPSection processNode(Rule r, Node e) {
 MPSection ms = new MPSection();
 ms.addComment(e.getId());
 e.getTags().stream().forEach((String s) -> {
 switch (s.split("=")[0]) {
 case "name":
 ms.addAttribute("Label=" + s.split("=")[1]);
 break;
 }
 ms.addComment(s);
 });
 ms.addAttribute("Type=" + r.code);
 ms.addAttribute("EndLevel=" + r.endLevel);
 ms.addCordinate(e.getCordinate());

 return ms;
 }*/

/*
 ; NodeID = 2383121120
 ; place = village
 [POI]
 Type=0xb00
 Label=Nochchiyagama
 EndLevel=4
 City=Y
 CityIdx=872
 Data0=(8.278638,80.237693)
 Population=5000
 [END]


 2015-10-28

 for (String k : n.getTags()) {
 tmp = myRule.subSet.get(k);
 if (tmp == null) {
 continue;
 }

 if (tmp.subSet.isEmpty()) {
 System.out.println(++i);
 mp.addPOI(cretePOI(tmp, n));

 } else {
 myRule = tmp;
 }
 }



                
                



 */
