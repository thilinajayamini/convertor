/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter.mp;

import java.util.List;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class MPRoadSection extends MPSection {

    String type;
    String endLevel;
    String roadID;
    char[] routeParam;
    String[] trafficLanes;
    String turnLanes;

    java.util.SortedMap<Integer, String> layer = new java.util.TreeMap<>();
    java.util.List<RoutingNode> routingNodes;

    public void setType(String type) {
        this.type = type;
    }

    public void setEndLevel(String endLevel) {
        this.endLevel = endLevel;
    }

    public void setRoadID(String roadID) {
        this.roadID = roadID;
    }

    public void setRouteParam(char[] RouteParam) {
        this.routeParam = RouteParam;
    }

    public void setTrafficLanes(String[] TrafficLanes) {
        this.trafficLanes = TrafficLanes;
    }

    public void setTurnLanes(String TurnLanes) {
        this.turnLanes = TurnLanes;
    }

    public void addLayerDetail(int index, String level) {
        this.layer.put(index, level);
    }

    public void addLayerDetails(java.util.SortedMap<Integer, String> detais) {
        this.layer.putAll(detais);
    }

    public void setRoutingNodes(List<RoutingNode> routingNodes) {
        this.routingNodes = routingNodes;
    }

    @Override
    public void print(MPWriter p, String Start, String end) {
        this.comments.stream().forEach((c) -> {
            p.print(";");
            p.println(c);
        });

        p.println(Start);
        p.println("Type=" + type);
        p.println("EndLevel=" + endLevel);
        p.print(buidData(this.data));

        p.println("RoadID=" + roadID);
        p.println("RouteParam=" + getRoutParams(routeParam));

        int nodeNumber = 0;
        for (RoutingNode rn : this.routingNodes) {
            p.println("Nod" + (nodeNumber++) + "=" + rn.getPosition() + "," + rn.getRouttingNodeId() + ",0");

        }
        if (!"0".equals(trafficLanes[0])) {
            p.println("TrafficLanes=" + trafficLanes[0] + ",0,0," + trafficLanes[3] + ",0,0");
        }
        if (turnLanes != null) {
            p.println("TurnLanes=" + turnLanes);
        }

        this.attributes.entrySet().stream().forEach((a) -> {
            p.println(a.getKey() + "=" + a.getValue());
        });

        if (this.layer.size() > 0) {
            p.print("HLevel0=");
            layer.entrySet().stream().forEach((e) -> {
                p.print("(" + e.getKey() + "," + e.getValue() + "),");
            });
            p.println("");
        }

        p.println(end);
    }

    private String getRoutParams(char[] arr) {
        StringBuilder sb = new StringBuilder();
        for (char b : arr) {
            sb.append(String.valueOf(b)).append(",");
        }
        sb.replace(sb.length() - 1, sb.length(), "");
        return sb.toString();
    }

}
