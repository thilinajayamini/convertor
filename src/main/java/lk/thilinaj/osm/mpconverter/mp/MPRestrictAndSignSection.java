/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter.mp;

import java.util.List;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class MPRestrictAndSignSection extends MPSection {

    private String fromWay;
    private String fromRoadId;
    //private int fromWayIdx = -1;

    private String toWay;
    private String toRoadId;
    //private int toWayIdx = -1;

    private String type;

    private List<String> via;
    private String viaWay;

    private final StringBuilder traffPoints;

    public MPRestrictAndSignSection() {
        super();
        via = new java.util.ArrayList<>();
        traffPoints = new StringBuilder();
    }

    public String getFromWay() {
        return fromWay;
    }

    public void setFromWay(String fromWay) {
        this.fromWay = fromWay;
    }

    public String getFromRoadId() {
        return fromRoadId;
    }

    public void setFromRoadId(String fromRoadId) {
        this.fromRoadId = fromRoadId;
    }

    public String getToWay() {
        return toWay;
    }

    public void setToWay(String toWay) {
        this.toWay = toWay;
    }

    public String getToRoadId() {
        return toRoadId;
    }

    public void setToRoadId(String toRoadId) {
        this.toRoadId = toRoadId;
    }

    public List<String> getVia() {
        return via;
    }

    public void setVia(List<String> via) {
        this.via = via;
    }

    public void addVia(String via) {
        this.via.add(via);
    }
    /*no need to find index any more fill restriction in road converter modifyied  
     public int getFromWayIdx() {
     return fromWayIdx;
     }

     public void setFromWayIdx(int fromWayIdx) {
     this.fromWayIdx = fromWayIdx;
     }

     public int getToWayIdx() {
     return toWayIdx;
     }

     public void setToWayIdx(int toWayIdx) {
     this.toWayIdx = toWayIdx;
     }*/

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getViaWay() {
        return viaWay;
    }

    public void setViaWay(String viaWay) {
        this.viaWay = viaWay;
    }

    public void addTraffPoint(int id) {
        if (traffPoints.length() == 0) {
            traffPoints.append(id);
        } else {
            this.traffPoints.append(",").append(id);
        }

    }

    @Override
    public void print(MPWriter p, String Start, String end) {
        this.comments.stream().forEach((c) -> {
            p.print(";");
            p.println(c);
        });
        p.println(Start);
        p.println("TraffPoints=" + this.traffPoints);
        p.println("TraffRoads=" + this.fromRoadId + "," + this.toRoadId);
        this.attributes.entrySet().stream().forEach((e) -> {
            p.println(e.getKey() + "=" + e.getValue());
        });
        p.println(end);
    }

}
/*
 [Restrict]
 Nod=68264
 TraffPoints=60712,68264,80400
 TraffRoads=38025,15703
 RestrParam=0,0,0,0,0,1,0,0
 Time=(W,0,1,1,1,1,1,0,08:00,18:00)
 [END-Restrict]


 [Restrict]
 Nod=68264
 TraffPoints=60712,68264,80400
 TraffRoads=38025,15703
 RestrParam=0,0,0,0,0,1,0,0
 Time=(W,1,0,0,0,0,0,1,08:00,14:00),(W,1,1,0,0,0,0,0,10:00,12:00)
 [END-Restrict]
 */
