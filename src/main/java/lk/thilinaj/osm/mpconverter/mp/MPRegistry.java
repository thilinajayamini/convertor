/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter.mp;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public interface MPRegistry {

    void fillAddress(MPSection ms);
}
