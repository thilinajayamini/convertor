/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter.mp;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class Address {

    private String houseNumber;
    private String street;
    private String place;
    private String suburb;
    private String region;
    private String country;
    private String zip;

    private boolean isDataPresent = false;
    private boolean isPlace = false;

    @Override
    public String toString() {
        if (this.isValidAddress()) {
            return "HouseNumber=" + houseNumber + "\nStreetDesc=" + street + "\nCityName=" + place + "\nRegionName=" + region + "\nCountryName=" + country + "\nZip=" + zip;
        }
        return "\nRegionName=" + region + "\nCountryName=" + country;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
        isDataPresent = true;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
        isDataPresent = true;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public boolean isIsPlace() {
        return isPlace;
    }

    public void markAsPlace() {
        this.isPlace = true;
    }

    public boolean isValidAddress() {
        return isDataPresent;
    }

}
