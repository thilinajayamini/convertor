/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter.mp;

import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;

import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Thilina Jayamini
 */
public class MP {

    java.util.ArrayList<MPSection> pois, polyLines, polygons;
    java.util.ArrayList<MPRestrictAndSignSection> restricts, signs;
    java.util.Map<String, java.util.List<RoutingNode>> mpRoads;
    private String headerFile;
    static long period = 0, start = 0, totoalTime = 0;

    java.util.Queue<String> s = new ArrayDeque<>();

    public MP(String imgHeader) {
        pois = new java.util.ArrayList<>();
        polyLines = new java.util.ArrayList<>();
        polygons = new java.util.ArrayList<>();
        restricts = new java.util.ArrayList<>();
        signs = new java.util.ArrayList<>();
        mpRoads = new java.util.HashMap<>();
        this.headerFile = imgHeader;
    }

    public void addPOI(MPSection s) {
        this.pois.add(s);
    }

    public void addPolyLine(MPSection s) {
        this.polyLines.add(s);
    }

    public void addPolygon(MPSection s) {
        this.polygons.add(s);
    }

    public void addRestrict(MPRestrictAndSignSection s) {
        this.restricts.add(s);
    }

    public void addSign(MPRestrictAndSignSection s) {
        this.signs.add(s);
    }

    public ArrayList<MPRestrictAndSignSection> getRestricts() {
        return restricts;
    }

    public ArrayList<MPRestrictAndSignSection> getSigns() {
        return signs;
    }

    public void addMpRoad(String roadId, java.util.List<RoutingNode> nodes) {
        this.mpRoads.put(roadId, nodes);
    }

    public void setHeaderFile(String headerFile) {
        this.headerFile = headerFile;
    }

    public void writeToFile(MPWriter p) {
        start = System.currentTimeMillis();

        showPrintingStatus(p, "\n\nStart writing file : ");
        writeHeadder(p);
        showPrintingStatus(p, "Header done : ");
        printFile("[POI]", "[END]", pois, p);
        showPrintingStatus(p, "POI done : ");
        printFile("[POLYLINE]", "[END]", polyLines, p);
        showPrintingStatus(p, "POLYLINE done : ");
        printFile("[POLYGON]", "[END]", polygons, p);
        showPrintingStatus(p, "POLYGON done : ");
        printFile("[Restrict]", "[END-Restrict]", restricts, p);
        showPrintingStatus(p, "Restrict done : ");
    }

    public void fillAddress(MPRegistry registry) {

        List<MPSection> addrs = this.polygons.stream().filter((p) -> (p.isValidAddress())).collect(Collectors.toList());
        addrs.addAll(this.pois.stream().filter((p) -> (p.isValidAddress())).collect(Collectors.toList()));
        addrs.stream().forEach((poi) -> {
            registry.fillAddress(poi);
        });

    }

    private void showPrintingStatus(MPWriter p, String msg) {
        period = (System.currentTimeMillis() - start) - totoalTime;
        totoalTime = System.currentTimeMillis() - start;
        p.printInfo(msg + " time => " + period );
    }

    public List<String> getRestrictionPoints() {
        return getJunctionNodes(restricts);
    }

    public List<String> getSignPoints() {
        return getJunctionNodes(signs);
    }

    private List<String> getJunctionNodes(List<MPRestrictAndSignSection> lst) {
        List<String> viaNodes = new java.util.ArrayList<>();
        lst.stream().forEach((r) -> {
            viaNodes.addAll(r.getVia());
        });
        return viaNodes;
    }

    private void printFile(String start, String end, java.util.ArrayList<? extends MPSection> sections, MPWriter p) {
        sections.stream().forEach((section) -> {
            p.println("\n");
            section.print(p, start, end);
            p.println("\n");
        });

        p.flush();
    }

    private void writeHeadder(MPWriter p) {
        try {
            java.io.FileInputStream in = new java.io.FileInputStream(this.headerFile);
            try (java.io.BufferedReader br = new java.io.BufferedReader(new InputStreamReader(in))) {
                String line;
                while (null != (line = br.readLine())) {
                    p.println(line);
                }
                p.println("\n");
            }
        } catch (Exception ex) {
            p.printError(ex.getMessage());
        }

    }

    /**
     *
     * @param roadId MP road id .
     * @param osmNodeId osm node id.
     * @return
     */
    public RoutingNode geRoutingNode(String roadId, String osmNodeId) {
        return this.mpRoads.get(roadId).stream().filter((rn) -> (rn.getOsmNodeId().equals(osmNodeId))).findAny().get();
    }

    /**
     *
     * @param roadId
     * @param index for + values result is form first node. for - values result
     * is from last node. for -1 will return last one
     * @return
     */
    public RoutingNode geRoutingNode(String roadId, int index) {
        if (index < 0) {
            List<RoutingNode> lst = this.mpRoads.get(roadId);
            index = lst.size() + index;
            return lst.get(index);
        }
        return this.mpRoads.get(roadId).get(index);
    }

    public List<RoutingNode> getRoutingNodesPerRoad(String roadId) {
        return this.mpRoads.get(roadId);
    }

}
