/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter.mp;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class MPWriter {

    private BufferedWriter bw;
    private PrintWriter error;

    /**
     *
     * @param inputFile
     * @param outputFile
     */
    public MPWriter(String inputFile, String outputFile) {
        try {
            bw = new BufferedWriter(new FileWriter(outputFile + ".mp"));
            //info = new PrintWriter(new BufferedOutputStream(System.out));
            error = new PrintWriter(outputFile + "-parsing_erros.log");
        } catch (IOException ex) {
            System.out.println("Error Creting output file " + ex.getMessage());
        }

    }

    public void print(String srt) {
        write(srt);
    }

    public void println(String srt) {
        write(srt);
        write(System.lineSeparator());
    }

    public void printError(String srt) {
        error.println(srt);
    }

    public void printInfo(String srt) {
        //info.println(srt);
        System.out.println(srt + System.lineSeparator());
    }

    private void write(String str) {
        try {
            bw.write(str);
        } catch (IOException ex) {
            System.out.println("Error writing to output file :" + ex.getMessage());
        }
    }

    public void flush() {
        try {
            bw.flush();
        } catch (IOException ex) {
            System.out.println("Error writing to output file :" + ex.getMessage());
        }
    }

    public void close() {
        try {
            bw.close();
            error.flush();
            error.close();
            //info.close();
        } catch (IOException ex) {
            System.out.println("Error writing to output file :" + ex.getMessage());
        }
    }

}
