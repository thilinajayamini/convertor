/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter.mp;

/**
 * Store id of the routing node in the graph,
 * position of the road nodes and respective osm node id.
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class RoutingNode {

    private int routtingNodeId;
    private int position;
    private String osmNodeId;

    /**
     *
     * @param routtingNodeId id of the graph.
     * @param position position of the road. 
     * @param osmNodeId respective osm node id.
     */
    public RoutingNode(int routtingNodeId, int position, String osmNodeId) {
        this.routtingNodeId = routtingNodeId;
        this.position = position;
        this.osmNodeId = osmNodeId;
    }

    public RoutingNode() {
    }

    public int getRouttingNodeId() {
        return routtingNodeId;
    }

    public void setRouttingNodeId(int routtingNodeId) {
        this.routtingNodeId = routtingNodeId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getOsmNodeId() {
        return osmNodeId;
    }

    public void setOsmNodeId(String osmNodeId) {
        this.osmNodeId = osmNodeId;
    }

}
