/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter.mp;

import lk.thilinaj.osm.parser.Coordinate;

/**
 * Implementation of basic mp file section
 *
 * @author Thilina Jayamini
 */
public class MPSection {

    java.util.List<String> comments;
    java.util.Map<String, String> attributes;
    java.util.List<java.util.List<Coordinate>> data;
    Coordinate adminCentre;
    //String name;
    boolean isPlace = false;
    boolean isValidAdd = false;

    public MPSection() {
        //address = new Address();
        this.comments = new java.util.ArrayList<>();
        this.attributes = new java.util.HashMap<>();
        this.data = new java.util.ArrayList<>();
        data.add(new java.util.ArrayList<>());
    }

    public void addComment(String s) {
        this.comments.add(s);
    }

    public java.util.List<String> getComments() {
        return this.comments;
    }

    public void addAttribute(String k, String v) {
        this.attributes.put(k, v);
    }

    public String getAttribute(String k) {
        return this.attributes.get(k);
    }

    public void deleteAttribute(String k) {
        this.attributes.remove(k);
    }

    public void addCordinate(Coordinate c) {
        this.data.get(0).add(c);
    }

    public Coordinate getCoordinate() {
        if (this.data.get(0).size() > 1 && this.adminCentre != null) {
            return getAdminCentre();
        }
        return this.data.get(0).get(0);
    }

    public void setAdminCentre(Coordinate c) {
        this.adminCentre = c;
    }

    private Coordinate getAdminCentre() {
        if (adminCentre == null) {
            return getCoordinate();
        }
        return adminCentre;
    }

    public void addDataList(java.util.List<Coordinate> lst) {
        if (this.data.size() == 1 && data.get(0).isEmpty()) {//to remove fist emplty array created in intialisation.
            data.remove(0);
        }
        this.data.add(lst);
    }

    public java.util.List<java.util.List<Coordinate>> getData() {
        return this.data;
    }

    public boolean isValidAddress() {
        return isValidAdd;
    }

    public void markAsValidAddress() {
        this.isValidAdd = true;
    }
    /*
     public void setAddress(Address address) {
     this.address = address;
     }

     public void setName(String name) {
     this.name = name;
     }

     public String getName() {
     return name;
     }*/

    public void markAsPlace() {
        this.isPlace = true;
    }

    public boolean isPlace() {
        return this.isPlace;
    }

    public void print(MPWriter p, String Start, String end) {
        java.util.List<String> add = new java.util.ArrayList<>();
        this.comments.stream().forEach((c) -> {
            p.print(";");
            p.println(c);
        });
        p.println(Start);
        p.print(buidData(this.data));
        add.add("RegionName=" + this.attributes.remove("RegionName"));
        add.add("CountryName=" + this.attributes.remove("CountryName"));
        this.attributes.entrySet().stream().forEach((e) -> {
            p.println(e.getKey() + "=" + e.getValue());
        });
        add.stream().forEach((a) -> {
            p.println(a);
        });
        p.println(end);

    }

    protected String buidData(java.util.List<java.util.List<Coordinate>> cl) {
        StringBuilder sb = new StringBuilder();

        cl.stream().forEach((c) -> {
            if (!c.isEmpty()) {
                sb.append("Data0=");
                c.stream().forEach((c1) -> {
                    sb.append("(").append(c1.getLat()).append(",").append(c1.getLon()).append(")").append(",");

                });
                sb.replace(sb.length() - 1, sb.length(), "\n");
            }
        });

        return sb.toString();
    }

}
