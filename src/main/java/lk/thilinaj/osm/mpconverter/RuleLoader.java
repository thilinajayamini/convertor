/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 *
 * @author Thilina Jayamini
 */
public class RuleLoader {

    public static Rule loadRules(Rule rMain, String fileName) throws Exception {
        if (rMain == null) {
            rMain = new Rule();
        }

        FileReader fr = new FileReader(fileName);
        try (BufferedReader br = new BufferedReader(fr)) {
            String line;
            String[] data, conditions;
            Rule r = rMain;
            Rule rTmp;
            while (null != (line = br.readLine())) {
                if (line.startsWith("#")) {
                    continue;
                }
                data = line.split(",");
                
                conditions = data[0].split(";");
                
                for (String c : conditions) {
                    
                    if (null == getRule(c, r)) {
                        
                        addRule(c, r);
                    }
                    rTmp = getRule(c, r);
                    rTmp.parent = r;
                    r = rTmp;
                    
                }
                r.code = data[1];
                r.endLevel = data[2];
                r.data = data[3];
                
                while (r.parent != null) {
                    rTmp = r.parent;
                    r = rTmp;
                }
            }
        }

        return rMain;

    }

    private static Rule getRule(String key, Rule r) {
        return r.subSet.get(key);
    }

    private static void addRule(String key, Rule r) {
        r.subSet.put(key, new Rule(key));
    }

    /*public static void main(String[] args) throws Exception {
     Rule nodes = new Rule();
     RuleLoader.loadRules(nodes, "nodes.csv");
     System.out.println("done");
     }*/
}

//tags should be in order 
//amenity=bank;building=yes,0x2F06,0
/*
 java.util.Map<String, Rule> ruleSet = new java.util.HashMap<>();

 void loadRules(String fileName) {
 String line;
 String[] data;
 String[] rules;
 String key, value;
 Rule rNew = null, rOld;

 try {
 FileReader fr = new FileReader(fileName);
 BufferedReader br = new BufferedReader(fr);

 while (null != (line = br.readLine())) {
 rNew = new Rule();

 if (!line.trim().equals("")) {
 data = line.split(",");//0->reules, 1-> code, 2-> end level
 rules = data[0].split(";");// 1..i rule -> key=value;key=value.....

 for (int x = 0; x < rules.length; x++) {
 key = rules[x].split("=")[0];// key
 value = rules[x].split("=")[1];// value

 if (x == 0) {
 rOld = ruleSet.get(key);
 if (rOld == null) {
 rOld = new Rule(key);
 ruleSet.put(key, rOld);
 }
 rNew = rOld.subSet.get(value);
 if (rNew == null) {
 rNew = new Rule(value);
 rOld.subSet.put(value, rNew);
 }
 }
 if (x > 0) {
 rOld = rNew.subSet.get(key);
 if (rOld == null) {
 rOld = new Rule(key);
 rNew.subSet.put(key, rOld);
 }
 rNew = rOld.subSet.get(value);
 if (rNew == null) {
 rNew = new Rule(value);
 rOld.subSet.put(value, rNew);
 }
 }
 }
 rNew.code = data[1];
 rNew.endLevel = data[2];
 }

 }

 } catch (IOException ex) {
 Logger.getLogger(Processor.class.getName()).log(Level.SEVERE, null, ex);

 }
 System.out.println("done");
 }*/
