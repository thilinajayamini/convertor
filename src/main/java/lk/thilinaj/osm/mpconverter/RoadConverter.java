/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter;

import lk.thilinaj.osm.parser.OSM;
import lk.thilinaj.osm.mpconverter.mp.MP;
import lk.thilinaj.osm.parser.Way;
import lk.thilinaj.osm.mpconverter.mp.MPRestrictAndSignSection;
import lk.thilinaj.osm.mpconverter.mp.MPRoadSection;
import lk.thilinaj.osm.mpconverter.mp.RoutingNode;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Thilina Jayamini
 */
class RoadConverter extends Converter {

    static int roadId = 1;
    //static Map<String, List<String>> startNds = new java.util.HashMap<>(), endNds = new java.util.HashMap<>();
    /**
     * node id and list of connected Way ids
     */
    static Map<String, List<String>> endPoints = new java.util.HashMap<>();//node id and list of connected Way ids
    static Map<String, String> layers = new java.util.HashMap<>();
    static Map<String, String> barries;
    static final String LAYER_IN_START = "layerS";
    static final String LAYER_IN_END = "layerE";

    static void convertRoads(OSM o, MP mp, Rule rRoad) {
        //List<String> wTypes = java.util.Arrays.asList(new String[]{"toll_booth", "gate","entrance"});//TODO load from settings add , "cattle_grid", "border_control"
        List<Way> roads = o.getWays().entrySet().parallelStream().
                filter((t) -> t.getValue().getTags().entrySet().stream().anyMatch((m) -> (m.getKey().equals("highway")))).
                map((t) -> (t.getValue())).collect(Collectors.toList());

        if (Config.applyBarriers) {
            List<String> bTypes = java.util.Arrays.asList(new String[]{"toll_booth", "gate", "entrance"});//TODO load from settings add , "cattle_grid", "border_control"
            barries = o.getNodes().entrySet().parallelStream().filter((e) -> e.getValue().getTags().entrySet().stream()
                    .anyMatch((b) -> (b.getKey().equals("barrier") && (!bTypes.contains(b.getValue())))))
                    //.map((n) -> (n.getKey())).collect(Collectors.toSet());
                    .collect(Collectors.toMap(n -> n.getValue().getId(), n -> n.getValue().getTags().get("barrier")));
        }

        o.clearWays();
        roads = formatRoads(roads);
        findStartsAndEnds(roads);

        Map<String, Way> roadMap = roads.stream().collect(Collectors.toMap(Way::getId, w -> addRoadID(w)));

        if (Config.enableLayes) {
            creteLayers(roads, roadMap);
        }

        mp.getRestrictionPoints().stream().forEach((rp) -> {
            addToEndPoints(rp, "");
        });

        createRoads(roads, o, mp, rRoad);

        o.clear();
        if (Config.applyRestrictions) {
            fillRestrictions(mp, roadMap);
        }

    }

    private static void createRoads(List<Way> roads, OSM o, MP mp, Rule rRoad) {
        NodeIndexer ndxer = new NodeIndexer();
        ndxer.fill(endPoints.keySet());

        roads.stream().forEach((rd) -> {
            List<RoutingNode> rNodes;
            Rule tr = findeRule(rd, rRoad);

            if (tr != null) {
                MPRoadSection ms = processRoad(tr, rd);
                ms.addDataList(rd.getNodes().stream().map((m) -> (o.getNodes().get(m).getCordinate())).collect(Collectors.toList()));

                int possitionOftheNode = 0;
                int indexOftheRoutingNode;
                rNodes = new java.util.ArrayList<>();

                for (String n : rd.getNodes()) {
                    indexOftheRoutingNode = ndxer.getIndex(n);
                    if (indexOftheRoutingNode > 0) {
                        rNodes.add(new RoutingNode(indexOftheRoutingNode, possitionOftheNode, n));
                    }
                    possitionOftheNode++;
                }
                ms.setRoutingNodes(rNodes);
                mp.addMpRoad(rd.getTags().get("road_id"), rNodes);//store the routing node and respective road id for restrictions and sign
                mp.addPolyLine(ms);
            }
        });
    }

    /*
     routing params
     0- speed,
     1- road_class,
     2- one_way,
     3- toll,
     4- denied_emergency,
     5- denied_delivery,
     6- denied_car,
     7- denied_bus,
     8- denied_taxi,
     9- denied_pedestrain,
     10- denied_bicycle,
     11- denied_truck
     */
    static MPRoadSection processRoad(Rule r, Way w) {
        MPRoadSection ms = new MPRoadSection();
        ms.addComment("type:way");
        ms.addComment("id:" + w.getId());

        char[] routingParams = r.data.toCharArray();
        String[] lanes = new String[]{"0", "0", "0", "0", "0", "0"};//[0] = lanes forward,[3]=lanes backward
        ms.setType(r.code);//To override in roundabout set early
        w.getTags().entrySet().stream().forEach((s) -> {

            int l = 0;
            if (s.getValue() != null) {

                switch (s.getKey()) {
                    case "maxspeed":
                        ms.addAttribute("LegalSpeed", s.getValue());
                        break;
                    case "name":
                        ms.addAttribute(Const.STREET, s.getValue());
                        ms.addAttribute(Const.NAME, s.getValue());
                        break;
                    case "oneway":
                        routingParams[2] = '1';
                        if (l > 0) {
                            lanes[0] = String.valueOf(l);
                            lanes[3] = "0";
                        }
                        break;
                    case "lanes":
                        l = parseLane(s.getValue());
                        lanes[0] = String.valueOf(l / 2);
                        lanes[3] = String.valueOf(l / 2);
                        break;
                    case "layer":
                        if (Config.enableLayes) {
                            ms.addLayerDetails(creteLayer(w.getNodes().size(), s.getValue()));
                        }
                        break;
                    case "turn:lanes":
                        ms.setTurnLanes(s.getValue());
                        break;

                    case LAYER_IN_START:
                        ms.addLayerDetail(0, s.getValue());
                        ms.addLayerDetail(1, "0");
                        break;
                    case LAYER_IN_END:
                        ms.addLayerDetail(w.getNodes().size() - 1, s.getValue());
                        ms.addLayerDetail(w.getNodes().size() - 2, "0");
                        break;

                    case "junction":
                        switch (s.getValue()) {
                            case "roundabout":
                                ms.setType("0x0c");
                                routingParams[2] = '1';
                                break;
                        }

                        break;
                    case "lanes:forward":
                        lanes[0] = s.getValue();
                        break;
                    case "lanes:backward":
                        lanes[3] = s.getValue();
                        break;
                    case "destination"://high way link or highway 
                        if (w.getTags().get("highway").endsWith("_link")) {//
                            ms.addAttribute(Const.NAME, s.getValue());
                        } else {
                            ms.addAttribute("Destination", s.getValue());
                        }
                        break;
                    /*case "is_in:city":
                     ms.addAttribute(Const.CITY, s.getValue());
                     break;*/
                    case "toll":
                        routingParams[3] = '1';
                        break;

                }
            }
            ms.addComment(s.getKey() + "=" + s.getValue());
        });
        ms.setRoadID(w.getTags().get("road_id"));
        ms.setRouteParam(routingParams);
        ms.setTrafficLanes(lanes);
        ms.setEndLevel(r.endLevel);

        return ms;
    }

    private static java.util.SortedMap<Integer, String> creteLayer(int nodes, String level) {

        java.util.SortedMap<Integer, String> hl = new java.util.TreeMap<>();

        for (int i = 0; i < nodes; i++) {
            hl.put(i, level);
        }
        return hl;
    }

    private static void findStartsAndEnds(List<Way> roads) {

        roads.stream().forEach(rd -> {
            String strt = rd.getNodes().get(0);
            String end = getLast(rd.getNodes());

            addToEndPoints(strt, rd.getId());

            addToEndPoints(end, rd.getId());

        });

        /*
         roads.stream().forEach(rd -> {
         String strt = rd.getNodes().get(0);
         String end = getLast(rd.getNodes());

         if (startNds.containsKey(strt)) {
         startNds.get(strt).add(rd.getId());
         } else {
         List<String> lst = new java.util.ArrayList<>();
         lst.add(strt);
         startNds.put(strt, lst);
         }

         if (endNds.containsKey(end)) {
         endNds.get(end).add(rd.getId());
         } else {
         List<String> lst = new java.util.ArrayList<>();
         lst.add(end);
         endNds.put(end, lst);
         }
         });*/
    }

    private static void addToEndPoints(String node, String road) {
        if (endPoints.containsKey(node)) {
            endPoints.get(node).add(road);
        } else {
            List<String> lst = new java.util.ArrayList<>();
            lst.add(road);
            endPoints.put(node, lst);
        }
    }

    /**
     *
     * @param roads
     * @param roadMap Map of wayId and Way
     */
    private static void creteLayers(List<Way> roads, Map<String, Way> roadMap) {
        List<Way> lrs = roads.stream().
                filter((t) -> t.getTags().entrySet().stream().anyMatch((m) -> (m.getKey().equals("layer")))).
                collect(Collectors.toList());

        lrs.stream().forEach((l) -> {
            String layerVal = l.getTags().get("layer");

            List<String> connected = endPoints.get(l.getNodes().get(0));//find ways start forn this start node
            connected.remove(l.getId());

            addStartOrEndLayer(connected, l.getNodes().get(0), layerVal, roadMap);

            connected = endPoints.get(getLast(l.getNodes()));
            connected.remove(l.getId());

            addStartOrEndLayer(connected, getLast(l.getNodes()), layerVal, roadMap);

        });
    }

    private static void addStartOrEndLayer(List<String> connected, String nodeId, String layerVal, Map<String, Way> roadMap) {
        connected.stream().forEach((c) -> {
            Way w = roadMap.get(c);

            if (w.getNodes().get(0).equals(nodeId)) {
                w.getTags().put(LAYER_IN_START, layerVal);
            } else {
                w.getTags().put(LAYER_IN_END, layerVal);
            }

        });

        /*
         connected.stream().forEach((c) -> {
         if (!c.equals(wayId)) {//avoid getting same way
         if (!layers.containsKey(c)) {
         int idx = 0;
         if (isEnd) {
         idx = findInList(roads, c).getNodes().size() - 1;
         }
         layers.put(c, "(" + idx + "," + layerVal + ")");
         }
         }
         });*/
    }

    private static <T extends lk.thilinaj.osm.parser.Element> T findInList(List<T> data, String id) {
        return data.stream().filter((d) -> (d.getId().equals(id))).findAny().get();
    }

    private static List<Way> formatRoads(List<Way> roads) {
        //TODO FIX close nodes        
        if (Config.fixSelfIntersection || Config.applyBarriers) {
            roads = fixSelfIntersectionAndBarriers(roads);
        }

        if (Config.splitRoads) {
            roads = splitRoads(roads);
        }

        return roads;
    }

    private static Way addRoadID(Way w) {
        w.getTags().put("road_id", String.valueOf(getRoadId()));
        return w;
    }

    private static synchronized int getRoadId() {
        return roadId++;
    }

    private static List<Way> splitRoads(List<Way> roads) {
        List<Way> splitedRoads = new java.util.ArrayList<>();
        roads.stream().forEach((r) -> {
            if (r.getNodes().size() > Config.maxRoadNodes) {
                splitedRoads.addAll(r.splitToSize(Config.maxRoadNodes));
            } else {
                splitedRoads.add(r);
            }
        });
        return splitedRoads;
    }

    private static List<Way> fixSelfIntersectionAndBarriers(List<Way> roads) {
        List<Way> splitedRoads = new java.util.ArrayList<>();

        roads.stream().forEach((r) -> {
            splitedRoads.addAll(splitOnSelfIntersectinsOrBarrier(r));
        });
        return splitedRoads;
    }

    private static List<Way> splitOnSelfIntersectinsOrBarrier(Way w) {
        List<Way> tmpLst = new java.util.ArrayList<>();
        List<Integer> toDelete = new java.util.ArrayList<>();
        String nd;
        boolean isSplited = false;
        int splitedIdx = 0;
        int x = w.length(), rdIdx = 0;
        for (int i = 0; i < x; i++) {
            nd = w.getNodes().get(i);
            if (Config.fixSelfIntersection) {
                for (int j = i + 1; j < x; j++) {
                    if (nd.equals(w.getNodes().get(j))) {
                        printError("Error duplicate node found in Way:" + w.getId() + " @ node[" + i + "]=" + nd + " is duplicate with [" + j + "]");
                        if (j - i == 1) {
                            toDelete.add(j);
                            printError("Info duplicate node deleted in Way:" + w.getId() + " @ node[" + j + "]");
                            continue;
                        }
                        isSplited = true;
                        if (rdIdx == 0) {
                            tmpLst.addAll(splitOnSelfIntersectinsOrBarrier(w.subWay(0, j, rdIdx++)));//from start to this possition
                        } else {
                            tmpLst.addAll(splitOnSelfIntersectinsOrBarrier(w.subWay(splitedIdx, j, rdIdx++)));//previos splited point to this possition
                        }
                        splitedIdx = j - 1;
                        i = j;
                        break;
                    }

                }
            }
            if (Config.applyBarriers && barries.containsKey(nd)) {

                if (!isSplited && 0 < i && i < x - 1) { //node is @ the middle of the way.need to split the way.
                    //printError("Barrier found in Way:" + w.getId() + " @ node[" + i + "]=" + nd + ". Creating restriction for it");
                    isSplited = true;
                    if (rdIdx == 0) {
                        tmpLst.addAll(splitOnSelfIntersectinsOrBarrier(w.subWay(0, i + 1, rdIdx++)));
                    } else {
                        tmpLst.addAll(splitOnSelfIntersectinsOrBarrier(w.subWay(splitedIdx, i + 1, rdIdx++)));
                    }
                    splitedIdx = i;
                }
            }//node is @ start or end of the way.no need to split the way.
        }
        if (isSplited) {
            tmpLst.add(w.subWay(splitedIdx, w.length(), rdIdx++));//if splited first part is alredy added to tmplist. now adding remaining part
        } else {
            tmpLst.add(w);//no split so add whole way to tmpList
        }
        for(int i : toDelete){
            w.getNodes().remove(i);
        }
        return tmpLst;
    }

    /**
     *
     * @param mp MP
     * @param roadMap Map of wayId and Way
     */
    public static void fillRestrictions(MP mp, Map<String, Way> roadMap) {

        mp.getRestricts().stream().forEach((r) -> {

            int index;
            if (r.getVia().size() == 1) {//TODO multi point restrictions
                List<String> connectedW = endPoints.get(r.getVia().get(0));//get connected way to given node

                index = connectedW.indexOf(r.getFromWay());
                if (index < 0) {//cannot find way from list because id store as way_id:spit eg 130024:1 format for splitted roads.
                    r.setFromRoadId(getComplexRoadId(roadMap, connectedW, r.getFromWay(), r.getVia().get(0)));
                } else {
                    r.setFromRoadId(roadMap.get(connectedW.get(index)).getTags().get("road_id"));
                }

                index = connectedW.indexOf(r.getToWay());
                if (index < 0) {
                    r.setFromRoadId(getComplexRoadId(roadMap, connectedW, r.getToWay(), r.getVia().get(0)));//ways with partition like wayid:1
                }
                r.setToRoadId(roadMap.get(connectedW.get(index)).getTags().get("road_id"));

                findTraffPoints(r, mp);

            } else {
                printError("\n\n\n\n\n here is multi point restriction please update code \n\n\n\n\n");
            }

        });
        creteRestrictionsFromBarriers(mp, roadMap);
    }

    private static void creteRestrictionsFromBarriers(MP mp, Map<String, Way> roadMap) {

        barries.entrySet().stream().forEach((b) -> {
            List<String> connected = endPoints.get(b.getKey());

            if (connected != null) {

                MPRestrictAndSignSection ms;

                for (int i = 0; i < connected.size(); i++) {//create restriction for each bariare for all directions.
                    for (int j = 0; j < connected.size(); j++) {

                        if (i != j) {
                            ms = new MPRestrictAndSignSection();
                            ms.setFromRoadId(roadMap.get(connected.get(i)).getTags().get("road_id"));
                            ms.setFromWay(roadMap.get(connected.get(i)).getId());
                            ms.setToRoadId(roadMap.get(connected.get(j)).getTags().get("road_id"));
                            ms.setToWay(roadMap.get(connected.get(j)).getId());
                            ms.addVia(b.getKey());
                            findTraffPoints(ms, mp);
                            ms.addComment("from barrier node:" + b.getKey() + "=" + b.getValue());
                            ms.addAttribute("RestrParam", "0,0,0,0,0,1,0,0");
                            mp.addRestrict(ms);
                        }
                    }
                }
            } else {

                printError("Error: Barrier is not on a way. Node id :" + b.getKey() + ". ignore barrier");
            }

        });
    }

    /*
     [Restrict]
     Nod=68264
     TraffPoints=60712,68264,80400 - from - via - to 
     TraffRoads=38025,15703
     RestrParam=0,0,0,0,0,1,0,0
     Time=(W,0,1,1,1,1,1,0,08:00,18:00)
     [END-Restrict]
     */
    private static void findTraffPoints(MPRestrictAndSignSection r, MP mp) {
        List<RoutingNode> fromRtNd = mp.getRoutingNodesPerRoad(r.getFromRoadId());
        List<RoutingNode> toRtNd = mp.getRoutingNodesPerRoad(r.getToRoadId());
        if (fromRtNd == null || toRtNd == null || fromRtNd.size() < 2 || toRtNd.size() < 2) {
            printError("Inncorect processing @ via :" + r.getVia().get(0) + " from: " + r.getFromWay() + " to: " + r.getToWay());
            if (fromRtNd != null) {
                printError("from: " + fromRtNd.size());
            }
            if (toRtNd != null) {
                printError("to: " + toRtNd.size());
            }

            return;
        }
        //TODO support multi point restrictions. currently only getvia(0) supports.
        //finding from and via 
        if (fromRtNd.get(0).getOsmNodeId().equals(r.getVia().get(0))) {//traff point is first routing node of the road. then add 2nd and 1st as from and via.
            r.addTraffPoint(fromRtNd.get(1).getRouttingNodeId());
            r.addTraffPoint(fromRtNd.get(0).getRouttingNodeId());

        } else {//traff point is last routing node of the road. then add last-1 and last as from and via.
            r.addTraffPoint(getByIndex(fromRtNd, -2).getRouttingNodeId());
            r.addTraffPoint(getByIndex(fromRtNd, -1).getRouttingNodeId());
        }

        //find to 
        if (toRtNd.get(0).getOsmNodeId().equals(r.getVia().get(0))) {//traff point is first routing node of the road. then 2nd as to.
            r.addTraffPoint(toRtNd.get(1).getRouttingNodeId());

        } else {//traff point is last routing node of the road. then add last-1 as to.
            r.addTraffPoint(getByIndex(toRtNd, -2).getRouttingNodeId());

        }
    }

    /**
     * search through way id list for matching way id(s) with given text. then
     * find Way in road map using above results. if there are more check nodes
     * for each Way if it contain via node. if contain via node return road id
     * of that way.
     *
     * @param roadMap map of wayId and Way
     * @param wayLst
     * @param wayId
     * @param viaNode OSM via node id
     * @return MP road id if found else null.
     */
    private static String getComplexRoadId(Map<String, Way> roadMap, List<String> wayLst, String wayId, String viaNode) {
        List<String> possibleWays = likeSearch(wayLst, wayId);
        if (possibleWays.size() == 1) {
            return roadMap.get(possibleWays.get(0)).getTags().get("road_id");
        }
        Way w;
        for (String possibleW : possibleWays) {
            w = roadMap.get(possibleW);
            if (w.getNodes().get(0).equals(viaNode) || getLast(w.getNodes()).equals(viaNode)) {//posible way 1st or last match with via
                return w.getTags().get("road_id");
            }
        }

        return null;
    }

    private static List<String> likeSearch(List<String> lst, String txt) {
        return lst.stream().filter(s -> (s.contains(txt))).collect(Collectors.toList());
    }

    private static int parseLane(String value) {
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            printError("Erro in parsing value" + value);
        }
        return 0;
    }

    static class NodeIndexer {

        java.util.Map<String, Integer> nodeMap;
        java.util.Map<String, Integer> indexMap;
        int index;

        public NodeIndexer() {
            index = 0;
            this.nodeMap = new java.util.HashMap<>();
            this.indexMap = new java.util.HashMap<>();
        }

        public void add(String n) {
            this.nodeMap.merge(n, 1, (v1, v2) -> v2 + 1);
        }

        public void add(String n, int i) {
            this.nodeMap.merge(n, i, (v1, v2) -> v2 + 1);
        }

        public void buildIndex() {
            this.nodeMap.keySet().stream().forEach((String n) -> {
                if (NodeIndexer.this.nodeMap.get(n) > 1) {
                    indexMap.put(n, indexMap.size() + 1);
                }
            });
            this.nodeMap = null;
        }

        public void fill(java.util.Set<String> nodeSet) {
            nodeSet.stream().forEach((nodeId) -> {
                indexMap.put(nodeId, indexMap.size() + 1);
            });
        }

        public void addEndOrStart(String n) {
            if (nodeMap.get(n) == null) {
                this.nodeMap.put(n, ++index);
            }
        }

        public int getIndex(String n) {
            return indexMap.getOrDefault(n, 0);
        }

        public void distroy() {
            this.indexMap = null;
        }

    }

}

/*
 this for bridges with hamp back
 for(i=0;i<lenth;i++){

 if(0<i*2< lenth){
 put(i,++hlevel)
 put(lenth-i-1,hlevel)
 }else{
 put(i,original_hlevel)



 ;332650441
 ;maxspeed=30
 ;name=NWSDB road
 ;highway=residential
 [POLYLINE]
 Type=0x6
 Label=NWSDB road
 EndLevel=1
 StreetDesc=NWSDB road
 RoadID=1317
 RouteParam=2,0,0,0,0,0,0,0,0,0,0,0
 Data0=(5.947847,80.533910),(5.947959,80.533910),(5.947992,80.533733),(5.947976,80.533562)
 Nod1=0,2301,0
 Nod2=3,1368,0
 HLevel0=(1,1),(2,1)
 LegalSpeed=30
 CGRoadSpeed=30
 [END]

 */

//List<Cordinate> collect = o.getWays().get(rd).getNodes().stream().map((m) -> (o.getNodes().get(m).getCordinate())).collect(Collectors.toList());
