/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.mpconverter;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class Rule {

    String name;
    String code;
    String endLevel;
    String data;//for roads this store routing params
    java.util.HashMap<String, Rule> subSet = new java.util.HashMap<>();
    Rule parent;

    public Rule(String name, String code, String endLevel) {
        this.name = name;
        this.code = code;
        this.endLevel = endLevel;
    }

    public Rule() {
    }

    public Rule(String name) {
        this.name = name;
    }

    public void setParent(Rule parent) {
        this.parent = parent;
    }

}
